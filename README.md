# HSPC Reference Apps

## Setup

HSPC Reference Apps is hosted as a static web app.
This example shows how to host it using a system such as http-server <https://www.npmjs.com/package/http-server>.

Install http-server
````
$ npm install http-server -g
````

## Hosting
````
$ http-server src -p 8086
````

The app is available for SMART Launch at:

* launch_url: <http://localhost:8086/launch.html>
* redirect_url: <http://localhost:8086/launch.html>
* image_url: <http://localhost:8086/images/appointments.png>